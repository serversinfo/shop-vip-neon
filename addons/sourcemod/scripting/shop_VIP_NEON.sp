#pragma semicolon 1
#include <sdktools>
#include <sdkhooks>
#include <vip_core>
#include <shop>
#include <clientprefs>

public Plugin myinfo =
{
	name	= "[Shop & VIP] NEON",
	author	= "R1KO & Pheonix & AlexTheRegent & ShaRen",
	version = "1.1.3",
	url		= "Servers-Info.Ru"
};

#define VIP_NEON_M			"NEON_MENU"
#define VIP_NEON			"NEON"

int g_iClientColor[MAXPLAYERS+1][4];
int g_iNeon[MAXPLAYERS+1];
Handle	g_hKeyValues_NeonData;			// shop
bool	g_bNeon[MAXPLAYERS+1];			// shop
Handle g_hKv, g_hmenu, g_hCookie;

public VIP_OnVIPLoaded()
{
	VIP_RegisterFeature(VIP_NEON_M, _, SELECTABLE, Open_aura, _, OnDrawItem);
	VIP_RegisterFeature(VIP_NEON, BOOL, _, OnToggleItem);
	VIP_HookClientSpawn(OnPlayerSpawn);
}

public OnPluginStart()
{
	g_hCookie = RegClientCookie("VIP_Neon", "VIP_Neon", CookieAccess_Public);

	HookEvent("player_death", Event_PlayerDeath);
	if ( Shop_IsStarted() )
		Shop_Started();
}

public OnPluginEnd()
{
	Shop_UnregisterMe();
}

public OnMapStart()
{
	decl String:buffer[256];
	if (g_hKv != INVALID_HANDLE) CloseHandle(g_hKv);
	g_hKv = CreateKeyValues("Neon_Colors");
	BuildPath(Path_SM, buffer, 256, "data/vip/modules/neon_colors.ini");
	if (!FileToKeyValues(g_hKv, buffer)) SetFailState("Couldn't parse file %s", buffer);
	g_hmenu = CreateMenu(AuraMenuHandler);
	SetMenuExitBackButton(g_hmenu, true);
	SetMenuTitle(g_hmenu, "Цвет неона\n ");
	KvRewind(g_hKv);
	if (KvGotoFirstSubKey(g_hKv)) {
		do {
			if (KvGetSectionName(g_hKv, buffer, 256))
				AddMenuItem(g_hmenu, buffer, buffer);
		} while (KvGotoNextKey(g_hKv));
	}
	KvRewind(g_hKv);
}

public VIP_OnVIPClientLoaded(iClient)
{
	if(VIP_IsClientFeatureUse(iClient, VIP_NEON)) {
		decl String:sInfo[64];
		GetClientCookie(iClient, g_hCookie, sInfo, 64);
		if(sInfo[0]) {
			//GetRGBAFromString(sInfo, g_iClientColor[iClient]);
			int len = 0;
			char Temp[16];

			for (int i=0; i<=3; i++) {
				len += BreakString(sInfo[len], Temp, 16);
				g_iClientColor[iClient][i] = StringToInt(Temp);

				if (g_iClientColor[iClient][i] < 0 || g_iClientColor[iClient][i] > 255)
					g_iClientColor[iClient][i] = 0;
			}
			return;
		}
	}
	for(new i=0; i < 4; i++)
		g_iClientColor[iClient][i] = 255;
}

public bool Open_aura(int iClient, const String:sFeatureName[])
{
	DisplayMenu(g_hmenu, iClient, MENU_TIME_FOREVER);
	return false;
}

public AuraMenuHandler(Handle:hMenu, MenuAction:action, iClient, Item)
{
	switch(action) {
		case MenuAction_Cancel:
			if(Item == MenuCancel_ExitBack)
				VIP_SendClientVIPMenu(iClient);
		case MenuAction_Select: {
			decl String:sInfo[64];
			GetMenuItem(hMenu, Item, sInfo, 64);
			KvRewind(g_hKv);
			if (KvJumpToKey(g_hKv, sInfo, false)) {
				KvGetColor(g_hKv, "color", g_iClientColor[iClient][0],  g_iClientColor[iClient][1],  g_iClientColor[iClient][2],  g_iClientColor[iClient][3]);
				KvRewind(g_hKv);
				PrintToChat(iClient, " \x03Вы изменили цвет неона на \x04%s", sInfo);
				FormatEx(sInfo, 64, "%i %i %i %i", g_iClientColor[iClient][0],  g_iClientColor[iClient][1],  g_iClientColor[iClient][2],  g_iClientColor[iClient][3]);
				SetClientCookie(iClient, g_hCookie, sInfo);
				if(VIP_IsClientFeatureUse(iClient, VIP_NEON))  SetClientNeon(iClient, false);
			} else PrintToChat(iClient, "Failed to use \"%s\"!.", sInfo);
			DisplayMenu(g_hmenu, iClient, MENU_TIME_FOREVER);
		}
	}
}

public OnDrawItem(iClient, const String:sSubMenuName[], style)
{
	if(VIP_GetClientFeatureStatus(iClient, VIP_NEON) == NO_ACCESS) return ITEMDRAW_DISABLED;
	return style;
}

public Action:OnToggleItem(iClient, const String:sFeatureName[], VIP_ToggleState:OldStatus, &VIP_ToggleState:NewStatus)
{
	if(NewStatus == ENABLED) SetClientNeon(iClient, false);
	else RemoveNeon(iClient);
	return Plugin_Continue;
}


////////		SHOP		/////

public Shop_Started()
{
	decl String:sPath[PLATFORM_MAX_PATH];
	Shop_GetCfgFile(sPath, sizeof(sPath), "neon.txt");
	g_hKeyValues_NeonData = CreateKeyValues("neon");
	if ( FileToKeyValues(g_hKeyValues_NeonData, sPath) ) {
		decl String:sBuffer[64], String:sBuffer2[64];
		KvGetString(g_hKeyValues_NeonData, "name", sBuffer, sizeof(sBuffer));
		KvGetString(g_hKeyValues_NeonData, "description", sBuffer2, sizeof(sBuffer2));
		CategoryId hCategoryId = Shop_RegisterCategory("neon", sBuffer, sBuffer2);
		if ( KvGotoFirstSubKey(g_hKeyValues_NeonData) ) {
			do {
				decl String:sSectionName[32];
				KvGetSectionName(g_hKeyValues_NeonData, sSectionName, sizeof(sSectionName));
				if ( Shop_StartItem(hCategoryId, sSectionName) ) {
					KvGetString(g_hKeyValues_NeonData, "name", sBuffer, sizeof(sBuffer));
					KvGetString(g_hKeyValues_NeonData, "description", sBuffer2, sizeof(sBuffer2));
					
					Shop_SetInfo(sBuffer, sBuffer2, KvGetNum(g_hKeyValues_NeonData, "price", 1000), KvGetNum(g_hKeyValues_NeonData, "sellprice", -1), Item_Togglable, KvGetNum(g_hKeyValues_NeonData, "duration", 604800));
					Shop_SetCallbacks(_, OnEquipItem);
					Shop_EndItem();
				}
			} while ( KvGotoNextKey(g_hKeyValues_NeonData) );
		}
	}
}

public ShopAction OnEquipItem(iClient, CategoryId:hCategoryId, const String:sCategory[], ItemId:hItemId, const String:sItem[], bool:bIsOn, bool:bElapsed)
{
	if ( bIsOn || bElapsed )
	{
		RemoveNeon(iClient);
		g_bNeon[iClient] = false;
		return Shop_UseOff;
	}
	
	KvRewind(g_hKeyValues_NeonData);
	if ( KvJumpToKey(g_hKeyValues_NeonData, sItem, false) ) {
		KvGetColor(g_hKeyValues_NeonData, "color", g_iClientColor[iClient][0], g_iClientColor[iClient][1], g_iClientColor[iClient][2], g_iClientColor[iClient][3]);
		g_bNeon[iClient] = true;
		
		SetClientNeon(iClient, true);
		return Shop_UseOn;
	}
	
	g_bNeon[iClient] = false;
	RemoveNeon(iClient);
	return Shop_UseOff;
}

public OnClientDisconnect(iClient)
{
	g_bNeon[iClient] = false;
	g_iNeon[iClient] = 0;
}

public OnPlayerSpawn(iClient, iTeam, bool:bIsVIP)
{
	if(bIsVIP && VIP_IsClientFeatureUse(iClient, VIP_NEON))
		SetClientNeon(iClient, false);
	else if ( g_bNeon[iClient] )
		SetClientNeon(iClient, true);
}

public Action:Event_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast)
{
	RemoveNeon(GetClientOfUserId(GetEventInt(event, "userid")));
}

RemoveNeon(iClient)
{
	if(g_iNeon[iClient] > 0 && IsValidEdict(g_iNeon[iClient])) {
		AcceptEntityInput(g_iNeon[iClient], "Kill");
		RemoveEdict(g_iNeon[iClient]);
	}
	g_iNeon[iClient] = 0;
}

SetClientNeon(iClient, bShop)
{
	RemoveNeon(iClient);
	if ( !bShop || (g_bNeon[iClient] && IsPlayerAlive(iClient) )) {
		char sColor[16];
		FormatEx(sColor, sizeof(sColor), "%d %d %d %d", g_iClientColor[iClient][0],  g_iClientColor[iClient][1],  g_iClientColor[iClient][2],  g_iClientColor[iClient][3]);
		float fClientPos[3], fPos[3];
		GetClientAbsOrigin(iClient, fClientPos);
		GetCollisionPoint(iClient, fPos);
		int iEnt = CreateEntityByName("light_dynamic");
		DispatchKeyValue(iEnt, "brightness",				"5");
		DispatchKeyValue(iEnt, "spotlight_radius",			"50");
		DispatchKeyValue(iEnt, "distance",					"150");
		DispatchKeyValue(iEnt, "style",						"0");
		DispatchKeyValue(iEnt, "_light",					sColor);
		FormatEx(sColor, sizeof(sColor), "shop_neon_%d",  iEnt);
		DispatchKeyValue(iEnt, "targetname", sColor); 
		SetEntPropEnt(iEnt, Prop_Send, "m_hOwnerEntity", iClient);
		if(DispatchSpawn(iEnt)) {
			TeleportEntity(iEnt, fClientPos, NULL_VECTOR, NULL_VECTOR);
			AcceptEntityInput(iEnt, "TurnOn");
			SetVariantString("!activator");
			AcceptEntityInput(iEnt, "SetParent", iClient, iEnt, 0);
			g_iNeon[iClient] = iEnt;
		} else g_iNeon[iClient] = 0;
	}
}

GetCollisionPoint(iClient, Float:fPos[3])
{
	decl Float:vOrigin[3], Float:vAngles[3];
	
	GetClientEyePosition(iClient, vOrigin);
	GetClientEyeAngles(iClient, vAngles);
	
	Handle trace = TR_TraceRayFilterEx(vOrigin, vAngles, MASK_SOLID, RayType_Infinite, TraceEntityFilterPlayer);
	
	if(TR_DidHit(trace)) {
		TR_GetEndPosition(fPos, trace);
		CloseHandle(trace);
		return;
	}
	CloseHandle(trace);
}

public bool:TraceEntityFilterPlayer(entity, contentsMask)
{
	return entity > MaxClients;
}